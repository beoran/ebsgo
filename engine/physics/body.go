package physics

import "gitlab.com/beoran/ebsgo/engine/geometry"

// Body simulates a physical body in 2D
type Body struct {
    P geometry.Vector // Position
    S geometry.Vector // Size of bounding box.
    V geometry.Vector // Velocity
    A geometry.Vector // Acceleration
    M float32         // Mass
}



 

