package fifi

import "os"
import "path/filepath"

import al "gitlab.com/beoran/al5go/al"


// Find files 


var DataPath string = ""

func isDataPathOK(path string) bool {
    fn := filepath.Join(path, "eruta.conf")
    file, err := os.Open(fn) 
    file.Close()
    return err == nil
}


func Initialize() bool {
    DataPath = os.Getenv("EBS_DATA")
    if isDataPathOK(DataPath) {
        return true
    }

    wd, err := os.Getwd()
    if err == nil { 
        DataPath = filepath.Join(wd, "data")
        if isDataPathOK(DataPath) {
            return true
        }
    }
    return false
}

func Map(name string) string {
    return filepath.Join(DataPath, name)
}


/* Fifi contain functionality that helps finding back the file resouces,
such as images, music, etc that theengine needs.

An important concept here is the "virtual path", that is the path under the 
location of the data directory. So, for example, if the data of the 
app is installed on Linux in /usr/share/app/data,
then a vpath of 
font/my_nice_font.ttf 
will be resolved as 
/usr/share/app/data/font/my_nice_font.ttf.  
Or, if, on another OS, the data of the app is installed in 
C:\Program Files\App\data
then the same vpath will refer to
C:\Program Files\App\data\font\my_nice_font.ttf.

So Fifi is a way to get OS-independence and location-independence of 
the data files all at once. 

For save files or scores, the vpath is similar, but relative to the
"writeable" directory of the application.

*/

func LoadBitmap(filename string) * al.Bitmap {
    return al.LoadBitmap(Map(filename))
}









