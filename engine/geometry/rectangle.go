package geometry

type Rectangle struct {
    X   float32
    Y   float32
    W   float32
    H   float32
}


func NewRectangle(x, y, w, h float32) Rectangle {
    r := Rectangle{ x, y, w, h } 
    return r
}

func (rect Rectangle) X2() float32 {
    return rect.X + rect.W
}

func (rect Rectangle) Y2() float32 {
    return rect.Y + rect.H
}

