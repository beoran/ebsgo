package geometry

type Vector struct {
    X   float32
    Y   float32
}


type V = Vector
type P = Vector
type Point = Vector

func NewVector(x, y float32) Vector {
    p := Vector{ x, y } 
    return p
}

func (v1 * Vector) Add(v2 Vector) * Vector {
    v1.X += v2.X
    v1.Y += v2.Y
    return v1
} 

func (v1 Vector) Sum(v2 Vector) Vector {
   return NewVector(v1.X + v2.X, v1.Y + v2.Y)
}

