package tile

// import "fmt"
// import "log"
import "gitlab.com/beoran/al5go/al"
// import "gitlab.com/beoran/ebsgo/engine/geometry/point"
import "gitlab.com/beoran/ebsgo/engine/physics"
// import "gitlab.com/beoran/ebsgo/engine/fifi"


/** A tile map is a tile based  map, roughly equivalent to a 
 * "level" that uses tiled panes for it's display. 
 */
type Map struct {
    Background    *al.Bitmap
    Panes       []*Pane
    Sets        []*Set
    GridW          int
    GridH          int
}

const ( 
    TEXTURE_TILE    = "tile"
    TEXTURE_PLAYER  = "play"
    TEXTURE_GEAR    = "gear"
    TEXTURE_WEAPON  = "arms"
    TEXTURE_FOE     = "foes"
    TEXTURE_ITEM    = "item"
)

/** Initializes a tile map */
func (tm * Map) Init(tw, th int) {
    tm.GridW = tw
    tm.GridH = th
    tm.Panes = nil
}

func NewMap(tw, th int) * Map{
    tm := &Map{}
    tm.Init(tw, th)
    return tm
}


/** Returns a pointer to the pane at index or NULL if out of range. */
func (tm * Map) Pane(index int) * Pane {
    if index < 0                { return nil; }
    if index >= len(tm.Panes)   { return nil; }
    return tm.Panes[index]
}


func (tm * Map) AddPane(pane * Pane) * Pane {
    tm.Panes = append(tm.Panes, pane)
    return pane
}

func (tm * Map) NewPane(set * Set, tw, th int, name string) * Pane {
    pane := NewPane(set, tw, th, name)
    return tm.AddPane(pane)
}

/** Returns a pointer to the pane at index or NULL if out of range. */
func (tm * Map) Set(index int) * Set {
    if index < 0                { return nil; }
    if index >= len(tm.Sets)   { return nil; }
    return tm.Sets[index]
}

func (tm * Map) AddSet(set * Set) * Set {
    tm.Sets = append(tm.Sets, set)
    return set
}

func (tm * Map) NewSet(sheet * al.Bitmap, tile_w, tile_h, firstgid int) * Set {
    set := NewSet(sheet, tile_w, tile_h, firstgid)
    return tm.AddSet(set)
}


/* Looks up the tile set to use for the tile, based on it's 
 * TMX index. this is done based on the firstgid varables of the tile set.
 * In case there are several matches the first mathing tile set is returned
 */
func (tm * Map) LookupTmxTileset(tmx_index int) * Set{
    for _, set := range tm.Sets {
        if  tmx_index >= set.FirstGID {
            return set
        }
    }
    return nil
}


/** Returns the tile in the tile map in the given layer at the given tile coords. */
func (tm * Map) Tile(l, x, y int) * Tile {
    pane   := tm.Pane(l)
    if pane == nil { return nil; }
    return pane.Tile(x, y)
}

/** Sets a tile in the tile map to the given tile. */
func (tm * Map) SetTile(l, x, y int, tile * Tile) * Tile {
    pane   := tm.Pane(l)
    if pane == nil { return nil }
    return pane.SetTile(x, y, tile)
}
/** Sets a tile in the tile map to the tile with the given index. */
func (tm * Map) SetTileIndex(l, x, y, index int) * Tile {
    pane   := tm.Pane(l)
    if pane == nil { return nil }
    return pane.SetTileIndex(x, y, index)
}


/** Draws a tile map. */
func (tm * Map) Draw(camera * physics.Camera) {
    // var floor * Pane
    for i := 0 ; i < len(tm.Panes) ; i ++ {
        pane := tm.Panes[i]
        if pane != nil {
            pane.DrawTiles(camera)
            if ( i % 2 ) == 0 {
                pane.DrawBlends(camera)
                // floor =  pane
            } else if (i % 2) == 1 {
                // pane.DrawShadowsOn(floor, camera)
            }
        }
    }
}

/** Updates the tile map. Currently this animates the tiles. */
func (tm * Map) Update(dt float64) {
    for i := 0 ; i < len(tm.Panes) ; i ++ {
        pane := tm.Panes[i]
        if pane != nil {
            pane.Update(dt)
        }
    }
}    

/** Closes the map after use. Loaded bitmaps are unloaded immediately. */
func (tm * Map) Close() {
    for i := 0 ; i < len(tm.Panes) ; i ++ {
        pane := tm.Panes[i]
        if pane != nil {
            pane.Close()
        }
        tm.Panes[i] = nil
    }
    for i := 0 ; i < len(tm.Sets) ; i ++ {
        pane := tm.Sets[i]
        if pane != nil {
            pane.Close()
        }
        tm.Sets[i] = nil
    }
}



/** Sets up the camera so it will stay locked in to the 
given layer of the tile map */

/*
Lockin * tilepane_lockin(Tilepane * pane, Camera * camera) {
  float x, y, w, h;
  if(!pane) return NULL;
  x = 0.0;
  y = 0.0;
  w = tilepane_wide(pane);  
  h = tilepane_high(pane);  
  return camera_newlockin(camera, x, y, w, h);
} 


Lockin * tilemap_layer_lockin(Tilemap * map, 
                              int layer, 
                              Camera * camera) {
  Tilepane * pane;
  Lockin * result;
  float x, y, w, h;
  if (!map) return NULL;
  pane = tilemap_pane(map, layer);
  return tilepane_lockin(pane, camera);  
}



Area * tilemap_area(Tilemap * self) {
  if(!self) return NULL;
  return self->area;
}

Thing * tilemap_thing(Tilemap * self, int index) {
  Area * area = tilemap_area(self);
  return area_thing(area, index);
}
*/


func (tm * Map) SetupAfterLoad() {
    for i := 0 ; i < len(tm.Panes) ; i ++ {
        pane := tm.Panes[i]
        if pane != nil {
            pane.InitBlend(i)
        }
    }    
}


