module ebsgo

require gitlab.com/beoran/al5go v0.0.0

require gitlab.com/beoran/ebsgo v0.0.0

require github.com/beevik/etree v1.0.1

replace gitlab.com/beoran/al5go => ../al5go

replace gitlab.com/beoran/ebsgo => ../ebsgo
