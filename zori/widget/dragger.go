package widget

import "gitlab.com/beoran/ebsgo/zori/event"
// import . "gitlab.com/beoran/ebsgo/zori/types"
// import _ "gitlab.com/beoran/ebsgo/zori/backend"
// import "gitlab.com/beoran/ebsgo/zori/state"
// import  "gitlab.com/beoran/ebsgo/zori/style"


type Draggable interface {
    event.MouseHandler
    Drag(x, y, start_w, start_y int)
    Drop(x, y, start_w, start_y int)
    CanDrag() bool
    IsInside(x, y int) bool
}

type Dragger struct {    
    Draggable
    Dragging bool
    StartX int
    StartY int
}


func (dr Dragger) MouseAxes(ev event.Mouse) event.Result {
    if dr.Draggable != nil {
        dr.Draggable.MouseAxes(ev)
        if dr.Draggable.CanDrag() && dr.Dragging {
            dr.Draggable.Drag(ev.X(), ev.Y(), dr.StartX, dr.StartY)
        }
    }
    return event.Result(false)
}


func (dr Dragger) MouseButtonPress(ev event.Mouse) event.Result {
    if dr.Draggable != nil {
        dr.Draggable.MouseButtonPress(ev)
        if (!dr.Draggable.IsInside(ev.X(), ev.Y()) || !dr.Draggable.CanDrag()) {
            return event.Result(false)
        }
        dr.Dragging = true
        dr.StartX   = ev.X()
        dr.StartY   = ev.Y()
        return event.Result(true)
    } else {
        return event.Result(false)
    }
}

func (dr Dragger) MouseButtonRelease(ev event.Mouse) event.Result {
    if dr.Draggable != nil {
        dr.Draggable.MouseButtonRelease(ev)
        if (!dr.Dragging) {
            return event.Result(false)
        }
        dr.Dragging = false
        dr.Draggable.Drop(ev.X(), ev.Y(), dr.StartX, dr.StartY)
        return event.Result(true)
    } else {
        return event.Result(false)
    }
}

