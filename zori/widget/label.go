package widget

// import "gitlab.com/beoran/ebsgo/zori/event"
import . "gitlab.com/beoran/ebsgo/zori/types"
// import _ "gitlab.com/beoran/ebsgo/zori/backend"
// import "gitlab.com/beoran/ebsgo/zori/state"
// import  "gitlab.com/beoran/ebsgo/zori/style"


/* Label widget. */
type Label struct {
    * Captioned
}

func NewLabel(parent Widget, bounds * Box, text string) * Label {
    lab := &Label{}
    lab.Captioned = NewCaptioned(parent, bounds, text)
    return lab
}

