package widget


import . "gitlab.com/beoran/ebsgo/zori/types"
import "gitlab.com/beoran/ebsgo/zori/event"
import "gitlab.com/beoran/ebsgo/zori/backend"
// import "gitlab.com/beoran/ebsgo/zori/state"
import  "gitlab.com/beoran/ebsgo/zori/style"
import  "gitlab.com/beoran/ebsgo/monolog"


/*
 * Root level widget, my spread out over several displays.
 * In Zori, there can only be a single root level widget active.
 * It's ID is always 0;
 */
type Root struct {
  * Basic
    Icons map[string]Bitmap    
    MyConsole Console
    UITheme style.Theme
    Cursors
    Active *PageWidget
}


func NewRoot (backend backend.Backend, theme * style.Theme) * Root {
    root := &Root{}
    if theme != nil {
        root.UITheme = * theme
    }
    root.Basic = NewBasicWithTheme(nil, nil, theme, nil)
    root.Be    = backend
    root.MyRoot= root // Root widget is self-rooted. 
    return root
}

func NewRootWidget(backend backend.Backend, theme * style.Theme) Widget {
    return Widget(NewRoot(backend, theme))
}

func (root * Root)  Dispatch(ev event.Event) event.Result {
    return DispatchRecursive(root, ev)
}

func (root * Root) Draw(ev event.Draw) event.Result {
    monolog.Debug("Root.Draw")
    return event.Pass
}

func (root * Root) Update(ev event.Update) event.Result {
    return event.Pass
}

func (root * Root) MouseCursor() Cursor {
    return root.Cursors.Mouse
}

func (root * Root) KeyjoyCursor() Cursor {
    return root.Cursors.Keyjoy
}


/* Gets the theme to use for a marked widget. Looks for the 
 * cursor to determine this. */
func (root Root) MarkedTheme() style.Theme {
    return root.Cursors.Keyjoy.TargetTheme
}

/* Gets the style to use for a hovered widget. Looks up the cursor 
 * to determine this. */
func (root Root) HoveredTheme() style.Theme {
    return root.Cursors.Mouse.TargetTheme
}


