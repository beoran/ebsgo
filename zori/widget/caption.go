package widget

import . "gitlab.com/beoran/ebsgo/zori/types"
// import _ "gitlab.com/beoran/ebsgo/zori/backend"
// import "gitlab.com/beoran/ebsgo/zori/state"
import  "gitlab.com/beoran/ebsgo/zori/style"
import  "gitlab.com/beoran/ebsgo/zori/event"
import  "gitlab.com/beoran/ebsgo/monolog"


/* Caption helper struct for use by widgets that have captions. */
type Caption struct {
    Label   string
    LabelTheme style.Theme
    LabelBox   Box
}

/* Widget with a caption. */
type Captioned struct {
    * Basic
    Caption
}

func (cap Captioned) Update(ev event.Update) event.Result {
    return event.Pass
}

func (cap Captioned) Draw(ev event.Draw) event.Result {
    them := cap.LabelTheme
    font := them.Text.Font
    colo := them.Text.Color
    lh  := font.LineHeight()
    y   := cap.Caption.LabelBox.X + lh
    x   := cap.Caption.LabelBox.Y + 5 // Needs to use margin
    cap.Be.DrawText(font, colo, x, y, cap.Label)
    monolog.Debug("Draw Caption: %d %d %s", x, y, cap.Label)
    return event.Pass
}

func NewCaptioned(parent Widget, bounds * Box, text string) * Captioned {
    cap := &Captioned{}
    cap.Basic = NewBasic(parent, bounds, nil)
    cap.Caption.Label = text
    cap.Caption.LabelTheme = * cap.Basic.Theme()
    if bounds != nil {
        cap.Caption.LabelBox = *bounds
    }
    return cap
}





