package widget

import "gitlab.com/beoran/ebsgo/zori/types"
import "gitlab.com/beoran/ebsgo/zori/event"
// import _ "gitlab.com/beoran/ebsgo/zori/backend"
// import "gitlab.com/beoran/ebsgo/zori/state"
// import  "gitlab.com/beoran/ebsgo/zori/style"
import  "gitlab.com/beoran/ebsgo/monolog"


type Button struct {
    * Captioned
}


/** Handles a mouse axis event and pass it on.  */
func (bu * Button) MouseAxes(ev event.Mouse) event.Result {
    if bu.IsInside(ev.X(), ev.Y()) {
        monolog.Info("Button motion: %d %d.\n", ev.X(), ev.Y())
        bu.SetHovered(true)
    } else {
        bu.SetHovered(false)
    }    
    return event.Pass
}
        
 
/** Handles a mouse click or activation and set the 
 * button and it's parent's result. */
func (bu * Button) Action(ev event.Action) event.Result { 
    bu.Result = interface{}(bu.ID)
    monolog.Info("Button clicked: %d.\n", bu.ID)
    
    menu, is_menu := bu.Parent().(*Menu)
    if is_menu {
        /* If the parent is a menu, also set it's result. */
        menu.Result = bu.ID
    }    
    /* Basic.Action will call the action callback. */
    return bu.Basic.Action(ev)
}

/** Handles a mouse click event and pass it on.  */
func (bu* Button) MouseButtonPress(ev event.Mouse) event.Result {
    if bu.IsInside(ev.X(), ev.Y()) { 
        monolog.Info("Mouse clicked: %v!\n", bu)
        ae := event.Action{}        
        return bu.Dispatch(ae)
    } else {
        monolog.Info("Mouse NOT clicked: %d %d in %d %d %d %d!\n", ev.X(), ev.Y(), bu.X, bu.Y, bu.W, bu.H )
    }
    return event.Pass
}

func (bu * Button) Dispatch(ev event.Event) event.Result {
    return DispatchRecursive(bu, ev)
}

func (bu * Button) Draw(ev event.Draw) event.Result {    
    bu.DrawBackground()
    bu.Captioned.Draw(ev)
    return event.Pass
}

func NewButton(parent Widget, bounds * types.Box, text string, cb ActionCallback) * Button {
    but := &Button{}
    but.Captioned = NewCaptioned(parent, bounds, text)
    return but
}

