package widget

// import "gitlab.com/beoran/ebsgo/zori/event"
// import . "gitlab.com/beoran/ebsgo/zori/types"
// import _ "gitlab.com/beoran/ebsgo/zori/backend"
// import "gitlab.com/beoran/ebsgo/zori/state"
// import  "gitlab.com/beoran/ebsgo/zori/style"


/* A descriptor for the current state of the longtext */
type LongtextState struct {
    /* Time waited between display of characters */
    wait float64
    /* Virtual text page we are currently on. */
    page int
    /* Line of text we are currently on. */
    line int
    /* Currently paused or not. */
    paused bool
    /* Total amount of lines to display for this text. */
    total int
    /* Current position in current line. */
    position int
    /* Animation timer. */
    animationTimer float64
}

type LongtextFlags int

const (
    /* Disable animation of pause marker. */
    LONGTEXT_FLAG_STATIC_MARKER = LongtextFlags(1 << 0)
    /*- Show letters one by one in stead of space separated words.  */
    LONGTEXT_FLAG_SHOW_LETTERS = LongtextFlags(1 << 1)
)

/* A descriptor for the configurable size settings of a
 * longtext. */
type LongtextSettings struct {
    /* Amount of lines to display in one virtual page of text. */
    Lines int
    /* Vertcal spacing between lines.  */
    Spacing int
    /* Delay between display of the individual characters. */
    Delay float64
    /* Several flags that modify the behavior of the box. */
    LongtextFlags
}

type Longtext struct {
    Captioned
    Text string
    LongtextSettings
    LongtextState
}


