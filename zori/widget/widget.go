package widget


import . "gitlab.com/beoran/ebsgo/zori/types"
import "gitlab.com/beoran/ebsgo/zori/backend"
import "gitlab.com/beoran/ebsgo/zori/state"
import  "gitlab.com/beoran/ebsgo/zori/style"
import  "gitlab.com/beoran/ebsgo/zori/event"


/* Mouse or keyboard/joystick cursor. */
type Cursor struct {
    Point
    Hover Widget
    Focus Widget
    Bitmap
    state.Flag
    style.Basic
    TargetTheme style.Theme
}

/* Support multiple cursors...  */
type Cursors struct {
    Mouse  Cursor
    Keyjoy Cursor
}


type Prioritized interface {
    Priority() int
}


type Backended interface {
    Backend() backend.Backend
}

type Parented interface {
    Parent() Widget
    Children() []Widget
}

type Rooted interface {
    Root() RootWidget
}

type Moveable interface {
    MoveBy(dx, dy int)
}


type Widget interface {
    Sized
    Positioned
    Prioritized
    style.Themed
    Backended
    Parented
    Rooted
    Moveable
    String() string
    event.Handler
}

type RootWidget interface {
    Widget
    MouseCursor() Cursor
    KeyjoyCursor() Cursor
    MarkedTheme() style.Theme 
    HoveredTheme() style.Theme
}


