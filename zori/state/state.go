package state

type Flag int

/* Generic state flags for several zori structs. */
const (
    HIDDEN      = Flag(1 << iota)
    DISABLED    = Flag(1 << iota)
    HOVERED     = Flag(1 << iota)
    MARKED      = Flag(1 << iota)
    READY       = Flag(1 << iota)
    DEACTIVATED = DISABLED | HIDDEN
)

