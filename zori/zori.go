package zori

// import "fmt"

// import "gitlab.com/beoran/ebsgo/monolog"
import "gitlab.com/beoran/ebsgo/zori/event"
import _ "gitlab.com/beoran/ebsgo/zori/types"
import "gitlab.com/beoran/ebsgo/zori/backend"
import _ "gitlab.com/beoran/ebsgo/zori/state"
import "gitlab.com/beoran/ebsgo/zori/style"
import "gitlab.com/beoran/ebsgo/zori/widget"



type UI struct {
  * widget.Root
    backend.Backend
}

func New(be backend.Backend, theme * style.Theme ) *UI {
    res := &UI{}    
    res.Backend = be
    res.Root = widget.NewRoot(res.Backend, theme)
    return res
}

func (ui * UI) Dispatch(ev event.Event) event.Result {
    return ui.Root.Dispatch(ev) 
}

func (ui * UI) Draw() event.Result {
    ev := event.Draw{}
    return ui.Root.Dispatch(ev) 
}

func (ui * UI) Update(dt float64) event.Result {
    ev := event.Update{}
    ev.TimePassed = dt
    return ui.Root.Dispatch(ev) 
}



