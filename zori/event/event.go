/* event handler and events for the Zori module. */

package event

import . "gitlab.com/beoran/ebsgo/zori/types"


// event types, intentionally same integer values as Allegro 5's event types
// apart from the Zori-specific events.
const (
   TypeJoystickAxis          =  1
   TypeJoystickButtonDown    =  2
   TypeJoystickButtonUp      =  3
   TypeJoystickConfiguration =  4   
   TypeKeyDown               = 10
   TypeKeyChar               = 11
   TypeKeyUp                 = 12   
   TypeMouseAxes             = 20
   TypeMouseButtonDown       = 21
   TypeMouseButtonUp         = 22
   TypeMouseEnterDisplay     = 23
   TypeMouseLeaveDisplay     = 24
   TypeMouseWarped           = 25   
   TypeTimer                 = 30   
   TypeDisplayExpose         = 40
   TypeDisplayResize         = 41
   TypeDisplayClose          = 42
   TypeDisplayLost           = 43
   TypeDisplayFound          = 44
   TypeDisplaySwitchIn       = 45
   TypeDisplaySwitchOut      = 46
   TypeDisplayOrientation    = 47
   TypeDisplayHaltDrawing    = 48
   TypeDisplayResumeDrawing  = 49   
   TypeTouchBegin            = 50
   TypeTouchEnd              = 51
   TypeTouchMove             = 52
   TypeTouchCancel           = 53
   TypeDisplayConnected      = 60
   TypeDisplayDisconnected   = 61
   TypeZoriUpdate            = 70311  
   TypeZoriResize            = 70312
   TypeZoriDraw              = 70313
   TypeZoriDestroy           = 70314
   TypeZoriAction            = 70315   
   TypeZoriClose             = 70316
   TypeZoriNewChild          = 70317
)


type Basic struct {
    Stamp  float64
    // Cannot be Widget type to avoid dependency loop, but should hold a widget or nil.
    EventWidget interface{}
    EventData   interface{}
}

func (be Basic) Data() interface{} {
    return be.EventData
}

func (be Basic) Widget() interface{} {
    return be.EventWidget
}

func (be Basic) Timestamp() float64 {
    return be.Stamp
}

/* Update event, when UI has to update (animations, etc). */
type Update struct {
    Basic
    TimePassed float64
}

func (be Update) Type() int {
    return TypeZoriUpdate
}


/* Resize event, when the parent of an element has resized. */
type Resize struct {
    Basic
    NewSize Box
}

func (be Resize) Type() int {
    return TypeZoriResize
}


/* Draw event when the UI has to draw itself. */
type Draw struct {
    Basic
}

func (be Draw) Type() int {
    return TypeZoriDraw
}


/* Cleanup event. */
type Destroy struct {
    Basic
}

func (be Destroy) Type() int {
    return TypeZoriDestroy
}


type ActionCallback func (data ... interface{}) Result

/* Action event. */
type Action struct {
    Basic
    ActionCallback
}

func (be Action) Type() int {
    return TypeZoriAction
}

/* Close event. */
type Close struct {
    Basic
    Parent interface {}
}

func (be Close) Type() int {
    return TypeZoriClose
}

/* New child event. */
type NewChild struct {
    Basic
    Child interface {}
}

func (be NewChild) Type() int {
    return TypeZoriNewChild
}


type Event interface {
    Type() int
    Timestamp() float64
}

type Zori interface {
    Event
    Data()   interface{}
    Widget() interface{}
}

type Joystick interface {
    Event
    ID() int
    Button() int
    Stick() int
    Axis() int
    Pos() float32
}

type Key interface {
    Event
    KeyCode() int
    Display() Display
    Unichar() rune
    Modifiers() int
    Repeat() bool
}


type Mouse interface {
    Event
    Display() Display
    X() int
    Y() int
    Z() int
    W() int
    DX() int
    DY() int
    DZ() int
    DW() int
    Button() int
    Pressure() float32
}

type Touch interface {
    Event
    Display() Display
    X() int
    Y() int
    DX() int
    DY() int
    ID() int
    Primary() bool
}

type Result bool

const Done    = Result(true)
const Pass    = Result(false)

type UIHandler interface {
    Update(ev Update) Result
    Resize(ev Resize) Result
    Draw(ev Draw) Result    
    Destroy(ev Destroy) Result
    Action(ev Action) Result
    Close(ev Close) Result
    NewChild(ev NewChild) Result
}

type JoystickHandler interface {
    JoystickButtonPress(ev Joystick) Result
    JoystickButtonRelease(ev Joystick) Result
    JoystickAxis(ev Joystick) Result
}

type KeyHandler interface {
    KeyPress(ev Key) Result
    KeyRelease(ev Key) Result
    KeyChar(ev Key) Result
}

type MouseHandler interface {
    MouseAxes(ev Mouse) Result
    MouseWarped(ev Mouse) Result
    MouseButtonPress(ev Mouse) Result
    MouseButtonRelease(ev Mouse) Result
    MouseEnterDisplay(ev Mouse) Result
    MouseLeaveDisplay(ev Mouse) Result
}

type TouchHandler interface {
    TouchBegin(ev Touch) Result
    TouchEnd(ev Touch) Result
    TouchMove(ev Touch) Result
    TouchCancel(ev Touch) Result
}


/* Handler is an interface for a type that can handle all 
 * of Zori's events. */
type Handler interface {
    UIHandler
    JoystickHandler
    KeyHandler
    MouseHandler
    TouchHandler
    Default(ev Event) Result
}


/* Dispatches joystick events. */
func DispatchJoystick(handler JoystickHandler, de Joystick) Result {
    switch de.Type() {
        case TypeJoystickButtonDown:    return handler.JoystickButtonPress(de)
        case TypeJoystickButtonUp:      return handler.JoystickButtonRelease(de)
        case TypeJoystickAxis:          return handler.JoystickAxis(de)
        default:                        return Pass
    }
}

/* Dispatches key events. */
func DispatchKey(handler KeyHandler, de Key) Result {
    switch de.Type() {
        case TypeKeyDown:    return handler.KeyPress(de)
        case TypeKeyUp:      return handler.KeyRelease(de)
        case TypeKeyChar:    return handler.KeyChar(de)
        default:             return Pass
    }
}


/* Dispatches mouse events. */
func DispatchMouse(handler MouseHandler, de Mouse) Result {
    switch de.Type() {
        case TypeMouseAxes:             return handler.MouseAxes(de)
        case TypeMouseWarped:           return handler.MouseWarped(de)
        case TypeMouseButtonDown:       return handler.MouseButtonPress(de)
        case TypeMouseButtonUp:         return handler.MouseButtonRelease(de)
        case TypeMouseEnterDisplay:     return handler.MouseEnterDisplay(de)
        case TypeMouseLeaveDisplay:     return handler.MouseLeaveDisplay(de)
        default:                        return Pass
    }
}


/* Dispatches touch events. */
func DispatchTouch(handler TouchHandler, de Touch) Result {
    switch de.Type() {
        case TypeTouchBegin:        return handler.TouchBegin(de)
        case TypeTouchEnd:          return handler.TouchEnd(de)
        case TypeTouchMove:         return handler.TouchMove(de)
        case TypeTouchCancel:       return handler.TouchCancel(de)
        default:                    return Pass
    }
}


func Dispatch(event Event, handler Handler) Result {
    switch de := event.(type) {
        case Update:                return handler.Update(de)
        case Draw:                  return handler.Draw(de)
        case Destroy:               return handler.Destroy(de)
        case Action:                return handler.Action(de)
        case Close:                 return handler.Close(de)
        case NewChild:              return handler.NewChild(de)
        case Joystick:              return DispatchJoystick(handler, de)
        case Key:                   return DispatchKey(handler, de)
        case Mouse:                 return DispatchMouse(handler, de)
        case Touch:                 return DispatchTouch(handler, de)
        default:                    return handler.Default(event)
    }
}

func String(event Event) string {
    switch event.(type) {
        case Update:             return "Update"
        case Draw:               return "Draw"
        case Destroy:            return "Destroy"
        case Action:             return "Action"
        case Close:              return "Close"
        case NewChild:           return "NewChild"
        case Joystick:           return "Joystick"
        case Key:                return "Key"
        case Mouse:              return "Mouse"
        case Touch:              return "Touch"
        default:                 return "DefaultEvent"
    }
}


