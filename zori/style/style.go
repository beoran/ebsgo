package style

import . "gitlab.com/beoran/ebsgo/zori/types"

type Flag int

const (
    BORDER  = Flag(1)
    FILL    = Flag(2)
    DEFAULT = Flag(3)
)

/**
 * For style: not all combinations make sense. How to improve this?
 *
 * What is available in CSS 1:
 *
 * font : font of the text (with many sub details in CCS not relevant here).
 * color: text color of an element
 * background-color: background color of an element.
 * background-image: background image of an element.
 * background-repeat: determines how/if the image is repeated.
 * background-attachment: fixed with regard to the canvas.
 * background-position: initial position of the background image.
 * word-spacing: text spacing
 * letter-spacing: text spacing
 * text-decoration: text decoration (underline, overline, etc)
 * vertical-align: vertical alignment, top, center, bottom based on the parent.
 * text-transform:
 * text-align: text alignment, left, right, centered.
 * text-indent: Text indentation.
 * line-height: Height of a line of text.
 * margin: sizes of the margin
 * padding: sizes of the padding
 * border-width: sizes of the border
 * border-color: color of the border
 * border-style: style of the border (per side)
 * border: border style, general of per side
 * width: width of the box
 * height: height of the box
 * float: floating
 * clear: float control
 * display: display type (visual, block, hiden, none, etc)
 * white-space: white space display.
 * list-style: list styling with type, image and position of the image.
 *
 * Squeezing it down to an essential subset we can retain the following:
 *
 * text.font    : Font for texts.
 * text.color   : Color for texts.
 * text.flags   : Style flags, merges horizontal alignment, decoration, etc.
 *
 * back.color   : Background color.
 * back.image   : Background image.
 * back.flags   : Style flags, solid background, no background, etc.
 * back.radius  : Size of corner for image_scale9 algorithm.
 *
 * border.color : Border color.
 * border.size  : Border size (thickness).
 * border.flags : Border style flags.
 * border.radius: Rounded corners radius.
 *
 * cursor.color   : Text cursor color.
 *
 * mouse.back     : Mouse cursor background information.
 * mouse.border   : Mouse cursor border.
 * keyjoy.back    : Keyjoy cursor background information.
 * keyjoy.border  : Keyjoy cursor border.
 *
 *
 * width, height, margins and padding are not considered style but positioning.
 * The keyjoy and mouse cursor and a border are only available
 * per-screen.
 *
 * The text cursor has a color only.
 *
 * 1. Text can have a font, a color and font flags (centered, etc)
 * 2. A rectangle / container / widget can have a background image combined
 * with a background color it can have draw flags for the background image and
 * for the color (fill or not). Furthermore it can have a border color.
 * Possibly (through not suupported now) would be a background
 *
 *
 */

type Basic struct {
    Color Color
    Bitmap
    Flag
    Radius int
}

/* The style of the background of a widget. */
type Background struct {
    Basic
}

/* The style of the border of a widget.  */
type Border struct {
    Basic
    Size int
}

/* A text style has all elements needed to style a piece of text.
 * It consists of the text color, font and font flags flags applied to a part of the GUI.
 */
type Text struct {
    Color
    Font
}

/* A theme is a set of style elements for a widget or cursor. */
type Theme struct {
    Background
    Border
    Text
}

type Themed interface {
    Theme() * Theme
}


type BasicTheme struct {
    MyTheme Theme
}

func (bs BasicTheme) Theme() * Theme {
    return & bs.MyTheme
}



