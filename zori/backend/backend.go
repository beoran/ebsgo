package backend

import . "gitlab.com/beoran/ebsgo/zori/types"
// import "gitlab.com/beoran/ebsgo/zori/event"


type Loader interface {
    LoadBitmap(name string) (Bitmap, error)
    LoadFont(name string, size int) (Font, error)
}

type Drawer interface {
    DrawBitmap(x, y int, bitmap Bitmap)
    DrawScaledBitmap(sx, sy, sw, sh, dx, dy, dw, dh int, bitmap Bitmap)
    DrawRectangle(x, y, w, h int, color Color, thickness int)
    DrawSlab(x, y, w, h int, color Color)
    DrawRoundedRectangle(x, y, w, h, rx, ry int, color Color, thickness int)
    DrawRoundedSlab(x, y, w, h, rx, ry int, color Color)
    DrawText(font Font, color Color, x, y int, text string)
    DrawMultilineText(font Font, color Color, x, y, max_width, line_height int, text string)    
}


type Displayer interface {
    Display() Display
}

type Backend interface {
    Displayer
    Loader
    Drawer
}

