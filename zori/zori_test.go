// zori_test.go
package zori

import (
    //    "strings"
    "testing"
    "gitlab.com/beoran/ebsgo/zori/event"
    . "gitlab.com/beoran/ebsgo/zori/types"
    _ "gitlab.com/beoran/ebsgo/zori/backend"

    // _ "gitlab.com/beoran/woe/monolog"
    // "gitlab.com/beoran/woe/tree"
)

func Assert(test *testing.T, ok bool, text string) bool {
    if !ok {
        test.Error(text)
    }
    return ok
}

type TestKeyboardEvent struct {
    event.Basic
    stamp   float64
    unichar rune
}

func (ke TestKeyboardEvent) IsKeyChar() {
}


func (ke TestKeyboardEvent) KeyCode() int {
    return int(ke.unichar)
}

func (ke TestKeyboardEvent) Unichar() rune {
    return ke.unichar
}

func (ke TestKeyboardEvent) Modifiers() int {
    return 0
}

func (ke TestKeyboardEvent) Repeat() bool {
    return false
}

func (ke TestKeyboardEvent) Display() Display {
    return nil
}

func TestDispatch(test *testing.T) {
    bw := &BasicWidget{}
    cw := &ConsoleWidget{}
    ev := &TestKeyboardEvent{}
    Assert(test, bool(event.Dispatch(ev, bw)),  "Dispatch bw failed")
    Assert(test, bool(event.Dispatch(ev, cw)),  "Dispatch cw failed")
    Assert(test, bool(bw.Dispatch(ev)),   "Dispatch bw failed")
    Assert(test, bool(cw.Dispatch(ev)),   "Dispatch cw failed")

}
