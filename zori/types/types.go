package types

type Sized interface {
    Width() int
    Height() int
}

type Positioned interface {
    Top() int
    Left() int
}

type Color interface {
}

type Point struct {
    X int
    Y int
}

func (p Point) Left() int {
    return p.X
}

func (p Point) Top() int {
    return p.Y
}

type Box struct {
    Point
    W int
    H int
}

func (re Box) Width() int {
    return re.W
}

func (re Box) Height() int {
    return re.H
}

type Bitmap interface {
    Sized
    Close()
}

type Font interface {
    TextWidth(text string) int 
    LineHeight() int
    Ascent() int
    Descent() int
    Close()
}

type Display interface {
    Sized
}


func Bounds(x,y, w, h int) Box {
    return Box{Point{x, y}, w, h}
}


