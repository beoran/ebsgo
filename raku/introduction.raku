# Introduction to the Raku scripting Language. This is a comment.
#--- And so is this.  
This is a multi line comment
---#

# Blank lines are generally ignored matter

# This is a simple command call with a string argument
say "hello"

# assignment and arithmetic operators are supported and keep their precedence
# floating point and integer numbers are supported 
a = 5 + 7.0 * -3.14

# Statements can be grouped between brackets or braces into blocks
# Block statements must be terminated by new lines or semicolons
[ say "First" ; say "Second" ; Say "third" ;]
{ say "First" ; say "Second" ; Say "third" ;}
{ 
    say "First" 
    say "Second"
    say "Third"
}

# A parenthesis block may only contain a single statement.
( a + sqrt 2.0 )  

# But it can be used as arguments to a command, or in an operator expression

z = ( a * sqrt 2.0 ) + (b * sqrt 3.0 ) 

# A block can be passed to a command as an argument

repeat 3 {
    say "Repeat"
}

a > b

# Control statements are just normal commands with block arguments
if ( a  > 10 ) { 
    say "Greater" 
} else { # Watch out, no newline is allowed before the "else", or the "else" would become a separate command.
    say "Lesser"
}

# Define a new command like this:
def say_hello [] {
    say "Hello!"
}



#---

set aa b 
set foobar to 10
# set the foobar's pianos' size to 10 + 10
open the door at the front .
cast cure at the big green dog behind the red door.
# loop for i from -1 to 10 do 
#    print i
#    print i * 2
# end
# check length of foo equals 5 and bar's size equals 7 do
#     print "foo OK"
# end else do
#     print "not foo OK"
# end
# 10 + 10 
# set foobar to the the 10 + 10
# type Being is an Object do
#    field its name is a String
#    field its hp is an Integer
#    field its hpx is an Integer 
# end


make Being 

# set foo expr not expr bar and baz end end
set foo to 10
cast cure light beastly fido
cast cure light at beastly fido
# set foo to ( 1 lshift 2 binand 78 rshift 1) greater_or_equal 77 

set bar to true or false
---#
