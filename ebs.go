package main

// import "fmt"
import "flag"
import "runtime"
import "gitlab.com/beoran/ebsgo/engine/global"
import "gitlab.com/beoran/ebsgo/engine/fifi"
import "gitlab.com/beoran/ebsgo/monolog"


// import "path/filepath"
// import "math/rand"

// some parameters
const SCREEN_W = 640
const SCREEN_H = 480
const WINDOW_TITLE = "Eruta Blessed Saviour"

// Log levels for monolog
type logLevels []string

var EbsLogLevels logLevels = logLevels{
    "FATAL", "ERROR", "WARNING", "INFO",
}

// Command line flags
var fullscreen_flag = flag.Bool("fullscreen", false, "Run fullscreen or not")
var enable_logs = flag.String("el", "FATAL,ERROR,WARNING,INFO", "Log levels to enable")
var disable_logs = flag.String("dl", "", "Log levels to disable")


func enableDisableLogs() {
    monolog.EnableLevels(*enable_logs)
    monolog.DisableLevels(*disable_logs)
    monolog.Info("Log levels: %v", monolog.DefaultLog.EnabledLevels())
}

func init() {
    runtime.LockOSThread()
}


func main() {
    flag.Parse()
    monolog.Setup("ebsgo.log", true, false)
    defer monolog.Close()
    enableDisableLogs()
    
    state := global.State{}
    if ! fifi.Initialize() {        
        monolog.Fatal("Could not find data file directory.")
        return
    }

    
    if ! state.InstallAllegro() {
        monolog.Fatal("Could not initialize allegro.")
        return
    }
    
    monolog.Info("Will open a screen, full screen: %B", *fullscreen_flag)
    
    if state.OpenDisplay(SCREEN_W, SCREEN_H, WINDOW_TITLE, *fullscreen_flag) == nil {
        monolog.Fatal("Error creating display.")
        return
    }
    
    state.Run()    
        
    /*    
    blue := CreateColor(0.0, 0.0, 1.0, 1.0)
    yellow := CreateColor(1.0, 1.0, 0.0, 1.0)
    ClearToColor(blue)
    DrawPixel(20.0, 10.0, yellow)
    FlipDisplay()
    Rest(1.0)
    display.SetWindowPosition(50, 100)
    ClearToColor(yellow)
    DrawPixel(20.0, 10.0, blue)
    FlipDisplay()
    display.Destroy()
    Rest(1.0)
    **/


}
